import React from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import ResultComponent from "./ResultComponent";

class MainComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            keyword: "",
            isLoading: false,
            response: [],
        };
        this.handleScrape = this.handleScrape.bind(this);
    }

    handleScrape() {
        if (this.state.keyword) {
            window.location.href =
                "/search/" + encodeURIComponent(this.state.keyword);
        } else {
            alert("Keyword required...");
        }
    }
    render() {
        return (
            <div className="container">
                <div className="row mt-3">
                    <div className="col-xl-8 offset-xl-2">
                        <div className="form-group">
                            <label>Kata Kunci</label>
                            <input
                                className="form-control"
                                type="text"
                                value={this.state.keyword}
                                placeholder="Masukkan kata kunci..."
                                onChange={(e) =>
                                    this.setState({ keyword: e.target.value })
                                }
                                onKeyPress={(e) => {
                                    if (
                                        e.key === "Enter" ||
                                        e.keyCode === 13 ||
                                        e.charCode === 13
                                    ) {
                                        this.handleScrape();
                                    }
                                }}
                            />
                        </div>
                        <button
                            className="btn btn-primary"
                            id="submit"
                            onClick={this.handleScrape}
                        >
                            Submit
                        </button>
                        {this.props.status ? (
                            <ResultComponent
                                data={JSON.parse(this.props.response)}
                            />
                        ) : null}
                    </div>
                </div>
            </div>
        );
    }
}

export default MainComponent;

if (document.getElementById("main")) {
    const propsContainer = document.getElementById("main");
    const props = Object.assign({}, propsContainer.dataset);

    ReactDOM.render(
        <MainComponent {...props} />,
        document.getElementById("main")
    );
}
